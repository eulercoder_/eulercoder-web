import React from 'react'
import Link from 'gatsby-link'
import Landing from '../components/landing' 
import Services from '../components/services' 
import Techstack from '../components/techstack'
import Clients from '../components/clients'
import ClientReview from '../components/clientreview'
import Launch from '../components/launch'

const IndexPage = () => (
  <div>
	<Landing />
    <Services />
	<Techstack />
	<Clients />
	<ClientReview/>
	<Launch />
  </div>
)

export default IndexPage
