import React from 'react'
import One from './1px.png'

const Techstack=()=>(

	<div className="techstack_wrap">
	<h1>Our Tech Stack</h1>
		<div className="techstack">
		
<div className="techstack_items">	<img className="spritereact" 	 src={One}  />		<p>react</p>		 </div>
<div className="techstack_items">	<img className="spriteaws" 		 src={One}  />			<p>aws</p>           </div>
<div className="techstack_items">	<img className="spritenode" 	 src={One}  />         <p>node</p>          </div>
<div className="techstack_items">	<img className="spritebootstrap" src={One}  />    <p>bootstrap</p>     </div>
<div className="techstack_items">	<img className="spriteangular" 	 src={One}	/>      <p>angular</p>       </div>
<div className="techstack_items">	<img className="spritejquery" 	 src={One}	/>       <p>jquery</p>        </div>
<div className="techstack_items">	<img className="spriteexpress" 	 src={One}	/>      <p>express</p>       </div>
<div className="techstack_items">	<img className="spritecss3" 	 src={One}	/>         <p>css3</p>          </div>
<div className="techstack_items">	<img className="spritedjango" 	 src={One}	/>       <p>django</p>        </div>
<div className="techstack_items">	<img className="spritedocker" 	 src={One}	/>       <p>docker</p>        </div>
<div className="techstack_items">	<img className="spritewebpack"	 src={One}	/>      <p>webpack</p>       </div>
<div className="techstack_items">	<img className="spritehtml5" 	 src={One}	/>        <p>html5</p>         </div>
<div className="techstack_items">	<img className="spritejs"		 src={One}	/>           <p>javascript</p>            </div>
<div className="techstack_items">	<img className="spritemongo" 	 src={One}	/>        <p>mongodb</p>         </div>
<div className="techstack_items">	<img className="spritepython" 	 src={One}	/>       <p>python</p>        </div>
<div className="techstack_items">	<img className="spritereact" 	 src={One}	/>        <p>react native</p>         </div>
<div className="techstack_items">	<img className="spriteandroid" 	 src={One}	/>      <p>android</p>       </div>
<div className="techstack_items">	<img className="spritejava" 	 src={One}	/>         <p>java</p>          </div>
	
		</div>
	</div>

)

export default Techstack