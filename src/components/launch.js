import React from 'react'
import launch from './launch.png'


const Launch = () => (
		<div className="launch_wrap">
		<h1>Ready to Launch</h1>
		
		<div className="launch">
		<img src={launch}/>
		<form>
		<input type="email" name="email" placeholder="email address" required/>
		<input type="text" name="phone" placeholder="phone number" />
		<input type="text" name="company" placeholder="company name" />
		<textarea rows="4" cols="25" placeholder="enter your messasge">
		</textarea>
		<button type="Submit">Send</button>
		</form>
		</div>
		</div>


)

export default Launch