import React from 'react'
import Link,{navigateTo} from 'gatsby-link' 
const Header_Cta=()=>(
	
	<div className="header_items_cta">
		<a href="tel:+91-81691-81238">IND:+91 81691 81238</a>
		<button onClick = {() => navigateTo('/')} >Let's talk</button>
	</div>
	

)

export default Header_Cta