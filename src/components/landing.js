import React from 'react'
import rocket from './ec_head_illustration_final.svg'

class Landing extends React.Component {
	constructor(props){
		super(props);
	} 
	
	componentDidMount(){
		window.addEventListener('scroll', this.handleScroll);
	}
	handleScroll(event){
		const land=document.querySelector('.landing');
		const head=document.querySelector('.header_comp');
		if(window.pageYOffset==0)
			{
				land.style.transition="opacity";
				land.style.opacity=1;
				head.style.transition="opacity";
				head.style.opacity=1;
				land.style.visibility="visible";
				head.style.visibility="visible";
			}
		if(window.pageYOffset>50 && window.pageYOffset<100)
			{
				land.style.transition="opacity 0.4s ease";
				land.style.opacity=0.8;
				head.style.transition="opacity 0.4s ease";
				head.style.opacity=0.8;
				land.style.visibility="visible";
				head.style.visibility="visible";
			}
		if(window.pageYOffset>99 && window.pageYOffset<300)
			{
				land.style.transition="opacity 0.4s ease";
				land.style.opacity=0.6;
				head.style.transition="opacity0.4s ease";
				head.style.opacity=0.6;
				land.style.visibility="visible";
				head.style.visibility="visible";
			}
		if(window.pageYOffset>299 && window.pageYOffset<500)
			{
				land.style.transition="opacity 0.4s ease";
				land.style.opacity=0.4;
				head.style.transition="opacity 0.4s ease";
				head.style.opacity=0.4;
				land.style.visibility="visible";
				head.style.visibility="visible";
			}
		if(window.pageYOffset>499)
			{
				land.style.transition="opacity 0.4s ease ";
				land.style.opacity=0;
				head.style.transition="opacity 0.4s ease ";
				head.style.opacity=0;
				land.style.visibility="hidden";
				head.style.visibility="hidden";
				
			}				
		 
	}
	
	render(){
	return(<div className="landing">
		<div className="landing_wrap">
			<h1>Building Successful Startups</h1>
			<p >Let's build something great.</p>
		</div>
		<img src={rocket} className="landing_img"/>
		<div className="arrow_wrap" >
		<button className="arrow_button" onClick={()=>{document.querySelector('.services').scrollIntoView({behavior: 'smooth'});}}>
		<div className="arrow_down"/></button>
		</div>
	</div>);
}
}

	
export default Landing