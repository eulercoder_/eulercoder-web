import React from 'react'
import Link from 'gatsby-link'
import Header_Items from './header_Items'
import Header_Cta from './header_Items_cta'
import logo from './eclogosvg.png'

class Header extends React.Component {
constructor(props){
	super(props);
}

componentDidMount(){
	document.querySelector('.ecmenu').addEventListener('click',this.dropMenu)
}
dropMenu(event){
		document.getElementsByClassName("dropdownmenu")[0].classList.toggle("appear");
		
		window.addEventListener("click" ,function(event) {
		  if ((!event.target.matches('.ecmenu'))) {

			const dropdowns = document.getElementsByClassName("dropdownmenu");
			let i;
			for (i = 0; i < dropdowns.length; i++) {
			  var openDropdown = dropdowns[i];
			  if (openDropdown.classList.contains('appear')) {
				openDropdown.classList.remove('appear');
			  }
			}
		  }
		});
		}


render(){
	return(
<div>
<div className="header_comp"
      style={{
        margin: '2px auto',
        maxWidth: 1300,
        padding: '1.5rem 1rem 1rem 1rem',
		
      }}
>
	
		<div>
				<Link
				  to="/"
				  style={{
					color: 'white',
					textDecoration: 'none',
					
				  }}
				>
				<span>
				<div className="ecmenu">
				<div className="ecdrop">
					<div className="ecmenu_lines"></div>
					<div className="ecmenu_lines"></div>
					<div className="ecmenu_lines"></div>
				</div>
				<div  className="dropdownmenu ">
						<Link to="/">Company</Link>
						<Link to="/">Services</Link>
						<Link to="/">Process</Link>
						<Link to="/">Portfolio</Link>
				</div>
				</div>
				</span>
				<span>
					<img className="eclogo" src={logo} width="45" />
				</span>
				<span>		
					<h1 className="eulercoderh1" >	
					  Eulercoder
					</h1>
				</span>	
				</Link>
				
				<span>
				 <Header_Items />
				 </span>
				 <span>
				 <Header_Cta/>
				 </span>
	</div>
		
</div>	
</div>
);}
}


export default Header
