module.exports = {
  siteMetadata: {
    title: 'Eulercoder',
  },
  plugins: ['gatsby-plugin-react-helmet'],
}
